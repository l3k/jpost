#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <ctime>
#include <sstream>

#include <boost/program_options.hpp>


int main(int argc, char** argv)
{
	
	bool titleGiven = false;
	bool authorGiven = false;
	bool tagsGiven = false;
	bool beVerbose = false;
	std::string title,author,tags;

	std::string jekyllMeta =
	"---\n"
	"title: \n"
	"author: \n"
	"tags: \n"
	"---\n";

// region commandline
	namespace po = boost::program_options;
	po::options_description desc("Allowed options");
	po::variables_map vm;    

	try
	{
		desc.add_options()
			("help,h", "Prints help screen")
			("verbose,v","Be verbose")
			("title,t", po::value<std::string>(&title)->required(),
								"Title for post generation [required]")
			("author,a", po::value<std::string>(&author), "Author of the post [optional]")
			("tags", po::value<std::string>(&tags), "Post tags [optional]");
	
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);
	
		if (vm.count("verbose")){
			beVerbose = true;
		}

		if (vm.count("title"))
		{
			if (beVerbose){    
				printf("title given: %s\n", title.c_str());
			}
			titleGiven = true;
		}
		
		if (vm.count("author"))
		{
			if (beVerbose){
				printf("author given: %s\n", author.c_str());
			}
			authorGiven = true;
		}

		if (vm.count("tags"))
		{
			if (beVerbose){
				printf("tags given: %s\n", tags.c_str());
			}
			tagsGiven = true;
		}	
	}
	catch (const boost::program_options::required_option & e)
	{
		if (vm.count("help"))
		{
			std::cout << desc << "\n";
			printf("example: jpost --title \"a fairly long post title\" --author \"me\" --tags \"test jekyll\"\n");
			return 0;
		}
		else
		{
			std::cout << e.what() << "\n";
			std::cout << desc << "\n";
			printf("example: jpost --title \"a fairly long post title\" --author \"me\" --tags \"test jekyll\"\n");
			return 1;
		}
	}
	catch (const boost::program_options::invalid_command_line_syntax & e)
	{
		std::cout << e.what() << "\n";
		std::cout << desc << "\n";
		printf("example: jpost --title \"a fairly long post title\" --author \"me\" --tags \"test jekyll\"\n");
		return 1;
	}
	catch (boost::program_options::unknown_option & e)
	{
		std::cout << e.what() << "\n";
		std::cout << desc << "\n";
		printf("example: jpost --title \"a fairly long post title\" --author \"me\" --tags \"test jekyll\"\n");
		return 1;
	}

// endregion commandline
	
	time_t now = time(0);
	tm *ltm = localtime(&now);
	std::ostringstream dteStrStream;
	dteStrStream << 1900 + ltm->tm_year << "-" << 1 + ltm->tm_mon << "-" << ltm->tm_mday << "-";

	std::string dteStr = dteStrStream.str();

	if (titleGiven)
	{
		auto t_pos = jekyllMeta.find("title: ");
		if (t_pos != std::string::npos){
			jekyllMeta.insert(t_pos + strlen("title: "), title);
		}

		auto md_pos = title.find(".markdown");
		if (md_pos == std::string::npos){
			title.append(".markdown");
		}

		auto space_pos = title.find(" ");
		while (space_pos != std::string::npos)
		{
			title.replace(space_pos, 1, "-");
			space_pos = title.find(" ");
		}
		title.insert(0,dteStr);
	}
	
	if (authorGiven)
	{
		auto a_pos = jekyllMeta.find("author: ");
		if (a_pos != std::string::npos){
			jekyllMeta.insert(a_pos + strlen("author: "), author);
		}
	}

	if (tagsGiven)
	{
		auto t_pos = jekyllMeta.find("tags: ");
		if (t_pos != std::string::npos){
			jekyllMeta.insert(t_pos + strlen("tags: "), tags);
		}
	}

	std::ofstream postfile;
	postfile.open (title);
	postfile << jekyllMeta.c_str();
	postfile.close();

	printf("Created file: %s\n", title.c_str());

	if (beVerbose){
		printf("Jekyll meta was: \n%s", jekyllMeta.c_str());
	}

	return 0;
}
