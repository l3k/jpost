[![pipeline status](https://gitlab.com/lilyyyy/jpost/badges/master/pipeline.svg)](https://gitlab.com/lilyyyy/jpost/-/commits/master)

### A simple Jekyll post generator that includes some metadata given as command line arguments.

----

#### Depends on make, CMake and boost-program-options

on Ubuntu install them with:

```
apt install build-essential cmake libboost-program-options-dev
```